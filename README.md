For START project :

Step1: Compile: 						gcc -pg main.c readfile.c -o workerDB
Step2: Running the executable file: 	./workerDB small.txt
Step3: For getting the analysis file: 	gprof workerDB gmon.out > analysis.txt


For USE the function in project :
1. Welcome page:
	use choice :
				1 -- to continue
				0 -- Exit
2. Home page :
	use choice :
				1 -- to printEmployeeDatabase;
				2 -- to lookupById;
				3 -- to lookupByLastName;
				4 -- to addEmplyee;
				5 -- to quit;
//
//  readfile.c
//  Lab1
//
//  Created by xiaofei xie on 4/15/19.
//  Copyright © 2019 xiaofei xie. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//we define employee maxum amount are 1024

FILE *file;

// use fopen to open file
int open_file(char *fileName){
    file = fopen (fileName, "r+");
    return file != NULL ? 0 : -1;
}

// the input is int
int read_int(int *i){
    return fscanf(file, "%d", i) == 1 ? 0 : -1;
}

// the input is char
int read_string(char s[64]){
    return fscanf(file, "%s", s) == 1 ? 0 : -1;
}

// the input is float
int read_float(float *f){
    return fscanf(file, "%f", f) == 1 ? 0 : -1;
}

// close the file
void close_file(){
    fclose(file);
}
